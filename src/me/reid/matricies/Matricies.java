package me.reid.matricies;

import java.util.Scanner;

public class Matricies {

	public static void main(String[] args) {

		Scanner console = new Scanner(System.in);

		int matrixArows, matrixAcols;
		System.out.println("Enter Matrix A's rows:");
		matrixArows = console.nextInt();
		System.out.println("Enter Matrix A's cols:");
		matrixAcols = console.nextInt();

		int[][] matrixA = new int[matrixArows][matrixAcols];

		System.out.println("Enter Matrix Data (" + matrixArows + "x" + matrixAcols + "):");
		for (int r = 0; r < matrixA.length; r++) {
			for (int c = 0; c < matrixA[r].length; c++) {
				matrixA[r][c] = console.nextInt();
			}
		}

		int matrixBrows, matrixBcols;
		System.out.println("Enter Matrix B's rows:");
		matrixBrows = console.nextInt();
		System.out.println("Enter Matrix B's cols:");
		matrixBcols = console.nextInt();

		int[][] matrixB = new int[matrixBrows][matrixBcols];

		System.out.println("Enter Matrix Data (" + matrixBrows + "x" + matrixBcols + "):");

		for (int r = 0; r < matrixB.length; r++) {
			for (int c = 0; c < matrixB[r].length; c++) {
				matrixB[r][c] = console.nextInt();
			}
		}

		// Multiplication between matrixA and matrixB
		if (matrixAcols != matrixBrows) {
			System.out.println("These matricies cannot be combined!");
			console.close();
			return;
		}

		int[][] matrix = new int[matrixArows][matrixBcols];
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[0].length; c++) {
				// Indexing each element matrix[r][c]
				int sum = 0;

				// Go Through Each Column of A
				for (int a = 0; a < matrixA[r].length; a++) {
					sum += (matrixA[r][a] * matrixB[a][c]);
				}

				matrix[r][c] = sum;
			}
		}

		System.out.println("");
		System.out.println("");
		System.out.println("Matrix:");
		display(matrixA);
		System.out.println("Times Matrix:");
		display(matrixB);
		System.out.println("Is Equal To:");
		display(matrix);

		console.close();

	}

	public static void display(int[][] matrix) {
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[r].length; c++) {
				System.out.print(matrix[r][c] + " ");
			}
			System.out.println("");
		}
	}

}
